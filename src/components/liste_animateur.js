import React, {Component} from 'react';
import {firestore} from "firebase";


class ListAnimateur extends Component {

    state = {
        animateur: [],
        mon_nombre : 0,
    };
    getAnimator = async () => {
        const db = firestore();
        let animateur = db.collection("animateurs");
        let animateurs = await animateur.get();
        this.setState({
            animateur: animateurs.docs,
        })
    };
    getAnimatorUpdate = async () => {
        const db = firestore();
        let animateur = db.collection("animateurs");
        let animateurs = await animateur.get();
        if(animateurs.docs.length){
            if(animateurs.docs.length !== this.state.animateur.length){
                this.getAnimator();
            }
        }
    };
    increment = () =>{
        this.setState({
            mon_nombre : this.state.mon_nombre + 1,
        })
    };
    componentDidMount()
    {
        console.log('componentDidMount');
        this.getAnimator();
    }
    render() {

        console.log('render');
        if (this.state.animateur.length) {
            this.getAnimatorUpdate();
            return (
                <div>
                    {
                        this.state.animateur.map((animate,index) => <div key={animate.id}>{animate.data().prenom}</div>)
                    }
                    <div>mon nombre  : {this.state.mon_nombre}</div>
                    <button onClick={this.increment}>increment</button>
                </div>
            )
        }
        else {
            return (
                <div>
                    <div>mon nombre  : {this.state.mon_nombre}</div>
                    Loading ...
                </div>
            )
        }
    }
}

export default ListAnimateur;