import React, {Component} from 'react';
import {NavLink} from 'react-router-dom';
import $ from 'jquery'


class Header extends Component {

    state = {
        hour: 0,
        min: 0,
        sec: 0
    };


    clock() {
        let myDate = new Date();
        this.setState({
            hour: myDate.getHours(),
            min: myDate.getMinutes(),
            sec: myDate.getSeconds()
        });
    }
     mobileMenu =()=> {
         let x = $("#demo");
         if (!x.hasClass("w3-show")) {
             x.addClass("w3-show");
         } else {
             x.removeClass("w3-show");
         }
    };
    menuDropdown =(id)=> {
        let x = $(id);
        if(x.hasClass("w3-show")){
            x.removeClass("w3-show");
        }else{
            $(".menu-drop").each(function(){
                $(this).removeClass("w3-show");
            });

            if (!x.hasClass("w3-show")) {
                x.addClass("w3-show");
            } else {
                x.removeClass("w3-show");
            }
        }
    };
    mMenuDropdown = (id)=>{
        let item = $(id +' > .sub-menu');

        item.each(function(){
            if($(this).hasClass("w3-hide")){
                $(this).removeClass("w3-hide");
            }else{
                $(this).addClass("w3-hide");
            }
        });

    }

    componentDidMount() {
        this.interval = setInterval(() => this.clock(), 1000);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    render() {

        return (
            <header className="w3-header w3-teal w3-center w3-bar ">
                <div>
                    <a href="#" className="w3-bar-item w3-button w3-right"><div>
                        {this.state.hour}:
                        {(this.state.min >= 10) ? this.state.min : '0' + this.state.min.toString()}:
                        {(this.state.sec >= 10) ? this.state.sec : '0' + this.state.sec.toString()}
                    </div></a>
                    <div className="w3-center">
                        <NavLink to="/list" className="w3-bar-item w3-button w3-hide-small" activeClassName="w3-light-grey">Dashboard</NavLink>
                        <div className="w3-dropdown-click w3-hide-small">
                            <button className="w3-button" onClick={()=>this.menuDropdown('#anim')}>
                                Animateur <i className ="fa fa-caret-down"> </i>
                            </button>
                            <div id="anim" className=" menu-drop w3-dropdown-content w3-bar-block w3-card-4">
                                <a href="#" className="w3-bar-item w3-button">Gérer les animateurs</a>
                                <a href="#" className="w3-bar-item w3-button">Ajouter un animateur</a>
                            </div>
                        </div>
                        <NavLink to="/insert" className="w3-bar-item w3-button w3-hide-small" activeClassName="w3-light-grey">Programme</NavLink>
                        <div className="w3-dropdown-click w3-hide-small">
                            <button className="w3-button" onClick={()=>this.menuDropdown('#emission')}>
                                Emission <i className ="fa fa-caret-down"> </i>
                            </button>
                            <div id="emission" className="menu-drop w3-dropdown-content w3-bar-block w3-card-4">
                                <a href="#" className="w3-bar-item w3-button">Créer une emission</a>
                                <a href="#" className="w3-bar-item w3-button">Gestion des émissions</a>
                                <a href="#" className="w3-bar-item w3-button">Podcast</a>
                            </div>
                        </div>
                        <div className="w3-dropdown-click w3-hide-small">
                            <button className="w3-button" onClick={()=>this.menuDropdown('#newz')}>
                                News <i className ="fa fa-caret-down"> </i>
                            </button>
                            <div id="newz" className="menu-drop w3-dropdown-content w3-bar-block w3-card-4">
                                <a href="#" className="w3-bar-item w3-button">Catégorie</a>
                                <a href="#" className="w3-bar-item w3-button">Article</a>
                            </div>
                        </div>
                        <a href="javascript:void(0)" className="w3-bar-item w3-button w3-right w3-hide-large w3-hide-medium" onClick={this.mobileMenu}>&#9776;</a>

                    </div>
                </div>

                <div id="demo" className="w3-bar-block w3-hide w3-hide-large w3-hide-medium" >
                    <NavLink to="/list" className="w3-bar-item w3-button " activeClassName="w3-light-grey">Dashboard</NavLink>
                    <div id="m-anim">
                        <button className="w3-button w3-bar-item " onClick={()=>this.mMenuDropdown('#m-anim')}>
                            Animateur <i className ="fa fa-caret-down"> </i>
                        </button>
                        <NavLink to="#" className="w3-bar-item w3-button sub-menu w3-hide" activeClassName="w3-light-grey"> > Gérer les animateurs</NavLink>
                        <NavLink to="#" className="w3-bar-item w3-button sub-menu w3-hide" activeClassName="w3-light-grey"> > Ajouter un animateur</NavLink>
                    </div>
                    <NavLink to="/insert" className="w3-bar-item w3-button " activeClassName="w3-light-grey">Programme</NavLink>
                    <div id="m-emission">
                        <button className="w3-button w3-bar-item" onClick={()=>this.mMenuDropdown('#m-emission')}>
                            Emission <i className ="fa fa-caret-down"> </i>
                        </button>
                        <NavLink to="#" className="w3-bar-item w3-button sub-menu w3-hide" activeClassName="w3-light-grey"> > Créer une emission</NavLink>
                        <NavLink to="#" className="w3-bar-item w3-button sub-menu w3-hide" activeClassName="w3-light-grey"> > Gestion des émissions</NavLink>
                        <NavLink to="#" className="w3-bar-item w3-button sub-menu w3-hide" activeClassName="w3-light-grey"> > Podcast</NavLink>
                    </div>
                    <div id="m-newz">
                        <button className="w3-button w3-bar-item" onClick={()=>this.mMenuDropdown('#m-newz')}>
                            News <i className ="fa fa-caret-down"> </i>
                        </button>
                        <NavLink to="#" className="w3-bar-item w3-button sub-menu w3-hide" activeClassName="w3-light-grey"> > Catégorie</NavLink>
                        <NavLink to="#" className="w3-bar-item w3-button sub-menu w3-hide" activeClassName="w3-light-grey"> > Article</NavLink>
                    </div>
                </div>

            </header>
        )
    }
}

export default Header;