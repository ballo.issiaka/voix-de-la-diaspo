import React,{Component} from 'react';
import {firestore} from "firebase"

class Animateur extends Component{
    constructor(){
        super();
        this.state = {
            biographie : "",
            nom: "",
            prenom :""
        };
    }
    bioUpdate = e =>{
        this.setState({
          biographie : e.target.value,
        });
    };
    nameUpdate = e =>{
        this.setState({
            nom : e.target.value,
        });
    };
    lnameUpdate = e =>{
        this.setState({
            prenom : e.target.value,
        });
    };
    addAnimator = e => {
        e.preventDefault();
        const db = firestore();
        db.collection("animateurs").add({
            biographie : this.state.biographie,
            nom : this.state.nom,
            prenom : this.state.prenom
        });
        this.setState({
            biographie: "",
            nom: "",
            prenom :"",
    });
    };
    render(){
        console.log('render');
        return(
            <form onSubmit={this.addAnimator}>
                <input
                    type="text"
                    name="biographie"
                    placeholder="biographie"
                    onChange={this.bioUpdate}
                    value={this.state.biographie}
                />
                <input
                    type="text"
                    name="nom"
                    placeholder="nom"
                    onChange={this.nameUpdate}
                    value={this.state.nom}
                />
                <input
                    type="text"
                    name="prenom"
                    placeholder="prenom"
                    onChange={this.lnameUpdate}
                    value={this.state.prenom}
                />
                <button type="submit">Submit</button>
            </form>
        );
    }
}
export default Animateur;