import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter, Route} from 'react-router-dom';
import {AnimatedSwitch} from 'react-router-transition'
import {initializeApp} from 'firebase';

//components imports
import Header from './components/header';
import Animateur from './components/animateur';
import ListAnimateur from './components/liste_animateur';

const config = {
    apiKey: "AIzaSyAmlvkAcI2g6ZAxNLzY7pIHFm0wHUzQoeE",
    authDomain: "voix-de-la-diaspora.firebaseapp.com",
    databaseURL: "https://voix-de-la-diaspora.firebaseio.com",
    projectId: "voix-de-la-diaspora",
    storageBucket: "voix-de-la-diaspora.appspot.com",
    messagingSenderId: "641803564087"
};
initializeApp(config);

class App extends Component {

    render() {
        return (

            <BrowserRouter>
                <div>
                    <Header/>
                    <h1>hello world</h1>
                    <h5>react training</h5>
                    <AnimatedSwitch
                    atEnter={{opacity : 0}}
                    atLeave={{opacity : 0}}
                    atActive={{opacity : 1}}
                    className="switch-wrapper">
                        <Route path="/insert" exact component={Animateur}/>
                        <Route path="/list" component={ListAnimateur}/>
                    </AnimatedSwitch>
                </div>
            </BrowserRouter>
        )
    }
}

ReactDOM.render(<App/>, document.querySelector('#root'));